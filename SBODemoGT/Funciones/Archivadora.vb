﻿Imports System
Imports System.Data.Entity
Imports System.IO
Imports System.Text
Imports Microsoft.Win32

Public Class Archivadora
    Public _dbo As New SBODemoGTEntities()

    ''' <summary>
    ''' Este método genera un resumen de todos los pedidos generados
    ''' </summary>
    Public Sub ResumenPedidos()
        Dim _stream As Stream
        Dim _streamWriter As StreamWriter
        Dim _filePath As String = "C:\Resumen Pedidos\ResumenPedidos.txt"
        Dim _grupo As String = ""
        Dim _nombre As String = ""
        Dim _total As String = ""
        System.IO.Directory.CreateDirectory("C:\Resumen Pedidos")

        _stream = File.OpenWrite(_filePath)
        _streamWriter = New StreamWriter(_stream, Encoding.UTF8)

        Dim query = (From x In _dbo.ORDR Select x Where x.docNum >= 1 Order By x.docTotal Descending).ToList
        _streamWriter.WriteLine("Nombre del Grupo           Cliente         Total")
        For Each x In query
            _grupo = x.OCRD.OCTG.groupName.ToString
            _nombre = x.OCRD.cardName
            _total = "Q" & x.docTotal
            _streamWriter.WriteLine(_grupo & "          " & _nombre & "         " & _total)
        Next
        _streamWriter.Close()
    End Sub

    ''' <summary>
    ''' Este método genera un resumen detallado filtrando por grupos
    ''' </summary>
    Public Sub FiltrarPorGrupo(ByVal _groupNum As Integer, ByVal _noRegistros As String)
        Dim _streamWriter As StreamWriter
        Dim _saveFileDialog As New SaveFileDialog()

        _saveFileDialog.Filter = "Documento de Texto (*.txt) | *.txt"
        _saveFileDialog.Title = "Guardar como"
        _saveFileDialog.ShowDialog()

        If _saveFileDialog.FileName <> "" Then
            Dim _fileStream As FileStream = CType _
                (_saveFileDialog.OpenFile(), FileStream)
            Select Case _saveFileDialog.FilterIndex
                Case 1
                    If _noRegistros = "TODOS" Then
                        _streamWriter = New StreamWriter(_fileStream, Encoding.UTF8)
                        Dim query = (From x In _dbo.RDR1 Select x Where x.ORDR.OCRD.groupNum = _groupNum And x.docNum >= 1
                                     Order By x.ORDR.docTotal Descending).ToList
                        For Each x In query
                            _streamWriter.WriteLine("Grupo: " & x.ORDR.OCRD.OCTG.groupName)
                            _streamWriter.WriteLine("No. Documento: " & x.docNum.ToString)
                            _streamWriter.WriteLine("Nombre del Cliente: " & x.ORDR.OCRD.cardName)
                            _streamWriter.WriteLine("Articulo comprado: " & x.OITM.itemName)
                            _streamWriter.WriteLine("Total: Q." & x.lineTotal)
                            _streamWriter.WriteLine("________________________________________")
                        Next
                        _streamWriter.Close()
                    Else
                        _streamWriter = New StreamWriter(_fileStream, Encoding.UTF8)
                        Dim query = (From x In _dbo.RDR1 Select x Where x.ORDR.OCRD.groupNum = _groupNum And x.ORDR.docNum >= 1 Take _noRegistros
                                     Order By x.ORDR.docTotal Descending).ToList

                        For Each x In query
                            _streamWriter.WriteLine("Grupo: " & x.ORDR.OCRD.OCTG.groupName)
                            _streamWriter.WriteLine("No. Documento: " & x.docNum.ToString)
                            _streamWriter.WriteLine("Nombre del Cliente: " & x.ORDR.OCRD.cardName)
                            _streamWriter.WriteLine("Articulo comprado: " & x.OITM.itemName)
                            _streamWriter.WriteLine("Total: Q." & x.lineTotal)
                            _streamWriter.WriteLine("________________________________________")
                        Next
                        _streamWriter.Close()
                    End If
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Este método genera un resumen detallado filtrando por fechas
    ''' </summary>
    Public Sub FiltrarPorFechas(ByVal _fechaAnterior As Date, ByVal _fechaPosterior As Date, ByVal _noRegistros As String)
        Dim _streamWriter As StreamWriter
        Dim _saveFileDialog As New SaveFileDialog()
        Dim _consulta As Boolean = False

        _saveFileDialog.Filter = "Documento de Texto (*.txt) | *.txt"
        _saveFileDialog.Title = "Guardar como"
        _saveFileDialog.ShowDialog()

        If _saveFileDialog.FileName <> "" Then
            Dim _fileStream As FileStream = CType _
                (_saveFileDialog.OpenFile(), FileStream)
            Select Case _saveFileDialog.FilterIndex
                Case 1
                    If _noRegistros = "TODOS" Then
                        _streamWriter = New StreamWriter(_fileStream, Encoding.UTF8)
                        Dim query = (From x In _dbo.RDR1 Select x Where (DbFunctions.TruncateTime(x.ORDR.docDate) >= DbFunctions.TruncateTime(_fechaAnterior)) _
                                                                      And (DbFunctions.TruncateTime(x.ORDR.docDate) <= DbFunctions.TruncateTime(_fechaPosterior)) _
                                                                      And x.docNum >= 1
                                     Order By x.ORDR.docTotal Descending).ToList
                        Try
                            For Each x In query
                                _streamWriter.WriteLine("Fecha & Hora: " & x.ORDR.docDate)
                                _streamWriter.WriteLine("No. Documento: " & x.docNum.ToString)
                                _streamWriter.WriteLine("Grupo: " & x.ORDR.OCRD.OCTG.groupName)
                                _streamWriter.WriteLine("Nombre del Cliente: " & x.ORDR.OCRD.cardName)
                                _streamWriter.WriteLine("Articulo comprado: " & x.OITM.itemName)
                                _streamWriter.WriteLine("Total: Q." & x.lineTotal)
                                _streamWriter.WriteLine("________________________________________")
                            Next
                            _streamWriter.Close()
                        Catch ex As Exception
                            _streamWriter.WriteLine("No existen Registros")
                            _streamWriter.Close()
                        End Try

                    Else
                        _streamWriter = New StreamWriter(_fileStream, Encoding.UTF8)
                        Dim query = (From x In _dbo.RDR1 Select x Where
                                                                      (DbFunctions.TruncateTime(x.ORDR.docDate) >= DbFunctions.TruncateTime(_fechaAnterior)) _
                                                                      And (DbFunctions.TruncateTime(x.ORDR.docDate) <= DbFunctions.TruncateTime(_fechaPosterior)) _
                                                                      And x.ORDR.docNum >= 1 Take _noRegistros
                                     Order By x.ORDR.docTotal Descending).ToList
                        For Each x In query
                            _streamWriter.WriteLine("Fecha & Hora: " & x.ORDR.docDate)
                            _streamWriter.WriteLine("No. Documento: " & x.docNum.ToString)
                            _streamWriter.WriteLine("Grupo: " & x.ORDR.OCRD.OCTG.groupName)
                            _streamWriter.WriteLine("Nombre del Cliente: " & x.ORDR.OCRD.cardName)
                            _streamWriter.WriteLine("Articulo comprado: " & x.OITM.itemName)
                            _streamWriter.WriteLine("Total: Q." & x.lineTotal)
                            _streamWriter.WriteLine("________________________________________")
                        Next
                        _streamWriter.Close()
                    End If
            End Select
        End If
    End Sub

    ''' <summary>
    ''' Este método genera un resumen detallado filtrando por codigo
    ''' </summary>
    Public Sub FiltrarPorCodigo(ByVal _numAtCard As Integer, ByVal _noRegistros As String)
        Dim _streamWriter As StreamWriter
        Dim _saveFileDialog As New SaveFileDialog()

        _saveFileDialog.Filter = "Documento de Texto (*.txt) | *.txt"
        _saveFileDialog.Title = "Guardar como"
        _saveFileDialog.ShowDialog()

        If _saveFileDialog.FileName <> "" Then
            Dim _fileStream As FileStream = CType _
                (_saveFileDialog.OpenFile(), FileStream)
            Select Case _saveFileDialog.FilterIndex
                Case 1
                    If _noRegistros = "TODOS" Then
                        _streamWriter = New StreamWriter(_fileStream, Encoding.UTF8)
                        Dim query = (From x In _dbo.RDR1 Select x Where x.ORDR.numAtCard = _numAtCard And x.docNum >= 1
                                     Order By x.ORDR.docTotal Descending).ToList
                        For Each x In query
                            _streamWriter.WriteLine("Codigo de Referencia: " & x.ORDR.numAtCard)
                            _streamWriter.WriteLine("No. Documento: " & x.docNum.ToString)
                            _streamWriter.WriteLine("Grupo: " & x.ORDR.OCRD.OCTG.groupName)
                            _streamWriter.WriteLine("Nombre del Cliente: " & x.ORDR.OCRD.cardName)
                            _streamWriter.WriteLine("Articulo comprado: " & x.OITM.itemName)
                            _streamWriter.WriteLine("Total: Q." & x.lineTotal)
                            _streamWriter.WriteLine("________________________________________")
                        Next
                        _streamWriter.Close()
                    Else
                        _streamWriter = New StreamWriter(_fileStream, Encoding.UTF8)
                        Dim query = (From x In _dbo.RDR1 Select x Where x.ORDR.numAtCard = _numAtCard And x.ORDR.docNum >= 1 Take _noRegistros
                                     Order By x.ORDR.docTotal Descending).ToList
                        For Each x In query
                            _streamWriter.WriteLine("Codigo de Referencia: " & x.ORDR.numAtCard)
                            _streamWriter.WriteLine("No. Documento: " & x.docNum.ToString)
                            _streamWriter.WriteLine("Grupo: " & x.ORDR.OCRD.OCTG.groupName)
                            _streamWriter.WriteLine("Nombre del Cliente: " & x.ORDR.OCRD.cardName)
                            _streamWriter.WriteLine("Articulo comprado: " & x.OITM.itemName)
                            _streamWriter.WriteLine("Total: Q." & x.lineTotal)
                            _streamWriter.WriteLine("________________________________________")
                        Next
                        _streamWriter.Close()
                    End If
            End Select
        End If
    End Sub

End Class
