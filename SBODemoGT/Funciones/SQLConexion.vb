﻿Imports System.Data.Entity

Public Class SQLConexion
    Shared _dbo As New SBODemoGTEntities()

#Region "CRUD de Clientes desde la Base de Datos"
    Shared Sub MostrarClientes(ByVal _grid As DataGrid)
        Dim query = (From x In _dbo.OCRD Select x).ToList
        _grid.ItemsSource = query
    End Sub

    Shared Sub CargarGrupos(ByVal _comboBox As ComboBox)
        Dim query = (From x In _dbo.OCTG Select x).ToList
        _comboBox.ItemsSource = query
        _comboBox.DisplayMemberPath = "groupName"
        _comboBox.SelectedValuePath = "groupNum"
        _comboBox.SelectedValue = -1
    End Sub

    Shared Sub AgregarClientes(ByVal _clientes As OCRD)
        _dbo.OCRD.Add(_clientes)
        _dbo.SaveChanges()
    End Sub

    Shared Function BuscarCliente(ByVal _cardCode As Integer) As OCRD
        Dim query = (From x In _dbo.OCRD Where x.cardCode = _cardCode Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarCliente(ByVal _cardCode As Integer, ByVal _cardName As String, ByVal _groupNum As Integer)
        Dim query = (From x In _dbo.OCRD Where x.cardCode = _cardCode Select x).SingleOrDefault
        With query
            .cardName = _cardName
            .groupNum = _groupNum
        End With
        _dbo.SaveChanges()
    End Sub

    Shared Sub EliminarCliente(ByVal _cardCode As Integer)
        Dim query = (From x In _dbo.OCRD Where x.cardCode = _cardCode Select x).SingleOrDefault
        _dbo.OCRD.Remove(query)
        _dbo.SaveChanges()
    End Sub
#End Region

#Region "CRUD de Grupo de Clientes desde la Base de Datos"
    Shared Sub MostrarGrupos(ByVal _grid As DataGrid)
        Dim query = (From x In _dbo.OCTG Select x).ToList
        _grid.ItemsSource = query
    End Sub

    Shared Sub AgregarGrupoClientes(ByVal _grupoClientes As OCTG)
        _dbo.OCTG.Add(_grupoClientes)
        _dbo.SaveChanges()
    End Sub

    Shared Sub EliminarGrupoClientes(ByVal _groupNum As Integer)
        Dim query = (From x In _dbo.OCTG Where x.groupNum = _groupNum Select x).SingleOrDefault
        _dbo.OCTG.Remove(query)
        _dbo.SaveChanges()
    End Sub

    Shared Function BuscarGrupoClientes(ByVal _groupNum As Integer) As OCTG
        Dim query = (From x In _dbo.OCTG Where x.groupNum = _groupNum Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarGrupoClientes(ByVal _groupNum As Integer, ByVal _groupName As String)
        Dim query = (From x In _dbo.OCTG Where x.groupNum = _groupNum Select x).SingleOrDefault
        With query
            .groupName = _groupName
        End With
        _dbo.SaveChanges()
    End Sub
#End Region

#Region "CRUD de Articulos desde la Base de Datos"
    Shared Sub MostrarArticulos(ByVal _grid As DataGrid)
        Dim query = (From x In _dbo.OITM Select x).ToList
        _grid.ItemsSource = query
    End Sub

    Shared Sub AgregarArticulos(ByVal _articulos As OITM)
        _dbo.OITM.Add(_articulos)
        _dbo.SaveChanges()
    End Sub

    Shared Sub EliminarArticulos(ByVal _itemCode As Integer)
        Dim query = (From x In _dbo.OITM Where x.itemCode = _itemCode Select x).SingleOrDefault
        _dbo.OITM.Remove(query)
        _dbo.SaveChanges()
    End Sub

    Shared Function BuscarArticulos(ByVal _itemCode As Integer) As OITM
        Dim query = (From x In _dbo.OITM Where x.itemCode = _itemCode Select x).SingleOrDefault
        Return query
    End Function

    Shared Sub ModificarArticulos(ByVal _itemCode As Integer, ByVal _itemName As String)
        Dim query = (From x In _dbo.OITM Where x.itemCode = _itemCode Select x).SingleOrDefault
        With query
            .itemName = _itemName
        End With
        _dbo.SaveChanges()
    End Sub
#End Region

#Region "CRUD de Pedidos desde la Base de Datos"
    Shared Sub MostrarPedidos(ByVal _grid As DataGrid)
        Dim query = (From x In _dbo.ORDR Select x Order By x.docNum Descending).ToList.Take(15)
        _grid.ItemsSource = query
    End Sub

    Shared Sub Consulta(ByVal _grid As DataGrid)
        Dim query = (From x In _dbo.RDR1 Select x Order By x.docNum Descending Take 15).ToList
        _grid.ItemsSource = query
    End Sub

    Shared Sub CargarComboCliente(ByVal _comboBox As ComboBox)
        Dim query = (From x In _dbo.OCRD Select x).ToList
        _comboBox.ItemsSource = query
        _comboBox.DisplayMemberPath = "cardName"
        _comboBox.SelectedValuePath = "cardCode"
        _comboBox.SelectedValue = -1
    End Sub

    Shared Sub CargarComboPedido(ByVal _comboBox As ComboBox)
        Dim query = (From x In _dbo.ORDR Order By x.docNum Descending Take 1 Select x).ToList
        Dim _seleccionarUltimo As Integer
        For Each x In query
            _seleccionarUltimo = x.docNum
        Next
        _comboBox.ItemsSource = query
        _comboBox.DisplayMemberPath = "docNum"
        _comboBox.SelectedValuePath = "docNum"
        _comboBox.SelectedValue = _seleccionarUltimo
    End Sub

    Shared Sub CargarComboArticulos(ByVal _comboBox As ComboBox)
        Dim query = (From x In _dbo.OITM Select x).ToList
        _comboBox.ItemsSource = query
        _comboBox.DisplayMemberPath = "itemName"
        _comboBox.SelectedValuePath = "itemCode"
        _comboBox.SelectedValue = -1
    End Sub

    Shared Sub AgregarPedido(ByVal _pedido As ORDR)
        _dbo.ORDR.Add(_pedido)
        _dbo.SaveChanges()
    End Sub

    Shared Sub AgregarVenta(ByVal _articulos As RDR1)
        _dbo.RDR1.Add(_articulos)
        _dbo.SaveChanges()
    End Sub

    Shared Function BuscarPedido(ByVal _docNum As Integer)
        Dim query = (From x In _dbo.ORDR Where x.docNum = _docNum Select x).ToList
        Return query
    End Function

    Shared Sub ModificarPedido(ByVal _docNum As Integer, ByVal _docTotal As Decimal)
        Dim query = (From x In _dbo.ORDR Where x.docNum = _docNum Select x).SingleOrDefault
        With query
            .docTotal = _docTotal
        End With
        _dbo.SaveChanges()
    End Sub
#End Region

#Region "Consultas para navegar con botones anterior y siguiente"
    Shared Function BuscarPedidoActual(ByVal _docEntry As Integer)
        Dim query = (From x In _dbo.RDR1 Where x.docEntry = _docEntry Select x).SingleOrDefault
        Return query
    End Function

    Shared Function BuscarSiguientePedido(ByVal _docEntry As Integer)
        Dim query = (From x In _dbo.RDR1 Where x.docEntry = _docEntry + 1 Select x).SingleOrDefault
        Return query
    End Function

    Shared Function BuscarAnteriorPedido(ByVal _docEntry As Integer)
        Dim query = (From x In _dbo.RDR1 Where x.docEntry = _docEntry - 1 Select x).SingleOrDefault
        Return query
    End Function

#End Region

End Class
