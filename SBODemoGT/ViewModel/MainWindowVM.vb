﻿Imports System.ComponentModel

Public Class MainWindowVM
    Implements ICommand, INotifyPropertyChanged

#Region "Variables declaradas"
    Private _mainWindow As MainWindow
    Private _mainWindowVM As MainWindowVM
    Private _seleccionarGrupo As New Object
    Private _seleccionarCliente As New Object
    Private _seleccionarArticulo As New Object
    Private _seleccionarPedido As New Object
    Private _fechaAnterior As Date
    Private _fechaPosterior As Date
    Private _nombre As String
    Private _codigo As Integer
    Private _nombreGrupo As String
    Private _nombreArticulo As String
    Private _noRegistros As String
#End Region

#Region "Constructor"
    Public Sub New(ByRef _instance As MainWindow)
        Me._mainWindow = _instance
        Me.VCMainWindow = Me
        _mainWindow.Width = SystemParameters.PrimaryScreenWidth * 0.7
        _mainWindow.Height = SystemParameters.PrimaryScreenHeight * 0.8
        MostrarClientes()
    End Sub
#End Region

#Region "Propiedades de la clase"
    Public Property VCMainWindow
        Get
            Return _mainWindowVM
        End Get
        Set(value)
            _mainWindowVM = value
        End Set
    End Property

    Public Property SeleccionarGrupoCliente
        Get
            Return _seleccionarGrupo
        End Get
        Set(value)
            _seleccionarGrupo = value
        End Set
    End Property

    Public Property SeleccionarCliente
        Get
            Return _seleccionarCliente
        End Get
        Set(value)
            _seleccionarCliente = value
        End Set
    End Property

    Public Property SeleccionarArticulo
        Get
            Return _seleccionarArticulo
        End Get
        Set(value)
            _seleccionarArticulo = value
        End Set
    End Property

    Public Property SeleccionarPedido
        Get
            Return _seleccionarPedido
        End Get
        Set(value)
            _seleccionarPedido = value
            'MsgBox(SeleccionarPedido.ToString)
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
            NotificarCambio(Nombre)
        End Set
    End Property

    Public Property NombreGrupo As String
        Get
            Return _nombreGrupo
        End Get
        Set(value As String)
            _nombreGrupo = value
            NotificarCambio(NombreGrupo)
        End Set
    End Property

    Public Property NombreArticulo As String
        Get
            Return _nombreArticulo
        End Get
        Set(value As String)
            _nombreArticulo = value
            NotificarCambio(NombreArticulo)
        End Set
    End Property

    Public Property NoRegistros As String
        Get
            Return _noRegistros
        End Get
        Set(value As String)
            _noRegistros = value
            NotificarCambio(NoRegistros)
        End Set
    End Property

    Public Property FechaAnterior As Date
        Get
            Return _fechaAnterior
        End Get
        Set(value As Date)
            _fechaAnterior = value
            NotificarCambio(FechaAnterior)
        End Set
    End Property

    Public Property FechaPosterior As Date
        Get
            Return _fechaPosterior
        End Get
        Set(value As Date)
            _fechaPosterior = value
            NotificarCambio(FechaPosterior)
        End Set
    End Property

    Public Property Codigo As Integer
        Get
            Return _codigo
        End Get
        Set(value As Integer)
            _codigo = value
            NotificarCambio(Codigo)
        End Set
    End Property
#End Region

#Region "Interfaz INotifyPropertyChanged / NotificarCambio"
    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter
            ''Navegar en Botones del Menu Inicio
            Case "Clientes"
                MostrarClientes()
            Case "GrupoClientes"
                MostrarGrupoClientes()
            Case "Articulos"
                MostrarArticulos()
            Case "Pedidos"
                MostrarPedidos()
            Case "Reportes"
                MostrarResumen()

            ''Navegar en Botones de Agregar, Modificar, Eliminar y Actualizar de Cliente
            Case "AgregarClientes"
                MostrarClientes()
                _mainWindow.ClienteBotones.Children.Add(_mainWindow.AgregarClientes)
                SQLConexion.CargarGrupos(_mainWindow.CBGrupoClientes)
            Case "ModificarClientes"
                MostrarClientes()
                BuscarCliente()
                _mainWindow.ClienteBotones.Children.Add(_mainWindow.ModificarClientes)
            Case "EliminarCliente"
                EliminarCliente()
            Case "ActualizarClientes"
                MostrarClientes()
            Case "AtrasClientes"
                MostrarClientes()
            Case "AgregarCliente"
                AgregarCliente()
            Case "ModificarCliente"
                ModificarCliente()

                ''Navegar en Botones de Agregar, Modificar, Eliminar y Actualizar de Grupos del Cliente
            Case "AgregarGrupoClientes"
                MostrarGrupoClientes()
                _mainWindow.GrupoBotones.Children.Add(_mainWindow.AgregarGrupo)
            Case "AtrasGrupoClientes"
                MostrarGrupoClientes()
            Case "EliminarGrupoCliente"
                EliminarGrupoCliente()
            Case "ActualizarGrupoClientes"
                MostrarGrupoClientes()
            Case "AgregarGrupoCliente"
                AgregarGrupoClientes()
                MostrarGrupoClientes()
            Case "ModificarGrupoClientes"
                BuscarGrupoClientes()
                MostrarGrupoClientes()
                _mainWindow.GrupoBotones.Children.Add(_mainWindow.ModificarGrupo)
            Case "ModificarGrupoCliente"
                ModificarGrupoClientes()
                MostrarGrupoClientes()

                ''Navegar en Botones de Agregar, Modificar, Eliminar y Actualizar de Articulos
            Case "AgregarArticulos"
                _mainWindow.ArticuloBotones.Children.Add(_mainWindow.AgregarArticulo)
            Case "ModificarArticulos"
                BuscarArticulo()
                _mainWindow.ArticuloBotones.Children.Add(_mainWindow.ModificarArticulo)
            Case "EliminarArticulo"
                EliminarArticulos()
            Case "ActualizarArticulos"
                MostrarArticulos()
            Case "AtrasArticulos"
                MostrarArticulos()
            Case "AgregarArticulo"
                AgregarArticulos()
            Case "ModificarArticulo"
                ModificarArticulo()

                ''Navegar en ventana pedidos
            Case "AgregarPedido"
                MostrarVentanaPedido()
            Case "ActualizarPedidos"
                MostrarPedidos()
            Case "SeleccionarPedido"
                DetallePedido()

                ''Resumenes navegar en botones de filtrado y resumenes
            Case "Resumen"
                ReporteResumen()
            Case "Filtrar"
                MostrarResumen()
                _mainWindow.ResumenBotones.Children.Add(_mainWindow.Filtrar)
            Case "AtrasResumen"
                MostrarResumen()
            Case "FiltrarDato"
                FiltrarRadioButton()
            Case "AtrasFiltrado"
                MostrarResumen()
                _mainWindow.ResumenBotones.Children.Add(_mainWindow.Filtrar)
            Case "FiltrarGrupos"
                ReporteGrupoDetallado()
            Case "ReporteFechas"
                ReporteFechaDetallado()
            Case "ReporteCodigo"
                ReporteCodigoDetallado()
        End Select
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
#End Region

#Region "Navegar en ventanas"
    Private Sub MostrarClientes()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Clientes)
        _mainWindow.Clientes.Children.Clear()
        _mainWindow.Clientes.Children.Add(_mainWindow.ClienteBotones)
        _mainWindow.ClienteBotones.Children.Remove(_mainWindow.AgregarClientes)
        _mainWindow.ClienteBotones.Children.Remove(_mainWindow.ModificarClientes)
        _mainWindow.Clientes.Children.Add(_mainWindow.TablaClientes)
        SQLConexion.MostrarClientes(_mainWindow.DatosClientes)
    End Sub

    Private Sub MostrarGrupoClientes()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.GrupoCliente)
        _mainWindow.GrupoCliente.Children.Clear()
        _mainWindow.GrupoCliente.Children.Add(_mainWindow.GrupoBotones)
        _mainWindow.GrupoBotones.Children.Remove(_mainWindow.AgregarGrupo)
        _mainWindow.GrupoBotones.Children.Remove(_mainWindow.ModificarGrupo)
        _mainWindow.GrupoCliente.Children.Add(_mainWindow.TablaGrupos)
        SQLConexion.MostrarGrupos(_mainWindow.DatosGrupos)
    End Sub

    Private Sub MostrarArticulos()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Articulos)
        _mainWindow.Articulos.Children.Clear()
        _mainWindow.Articulos.Children.Add(_mainWindow.ArticuloBotones)
        _mainWindow.ArticuloBotones.Children.Remove(_mainWindow.AgregarArticulo)
        _mainWindow.ArticuloBotones.Children.Remove(_mainWindow.ModificarArticulo)
        _mainWindow.Articulos.Children.Add(_mainWindow.TablaArticulos)
        SQLConexion.MostrarArticulos(_mainWindow.DatosArticulos)
    End Sub

    Private Sub MostrarPedidos()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Pedidos)
        _mainWindow.Pedidos.Children.Clear()
        _mainWindow.Pedidos.Children.Add(_mainWindow.PedidoBotones)
        _mainWindow.Pedidos.Children.Add(_mainWindow.TablaPedidos)
        SQLConexion.Consulta(_mainWindow.DatosPedidos)
    End Sub

    Private Sub MostrarResumen()
        _mainWindow.Principal.Children.Clear()
        _mainWindow.Anunciar.Text = ""
        _mainWindow.Principal.Children.Add(_mainWindow.Botones)
        _mainWindow.Principal.Children.Add(_mainWindow.Resumen)
        _mainWindow.Resumen.Children.Clear()
        _mainWindow.Resumen.Children.Add(_mainWindow.ResumenBotones)
        _mainWindow.ResumenBotones.Children.Remove(_mainWindow.Filtrar)
        _mainWindow.ResumenBotones.Children.Remove(_mainWindow.FiltrarGrupos)
        _mainWindow.ResumenBotones.Children.Remove(_mainWindow.FiltrarFechas)
        _mainWindow.ResumenBotones.Children.Remove(_mainWindow.FiltrarCodigos)
        _mainWindow.Resumen.Children.Add(_mainWindow.TablaResumen)
        SQLConexion.Consulta(_mainWindow.DatosResumen)
    End Sub

    Public Sub FiltrarRadioButton()
        If _mainWindow.RadioFechas.IsChecked = True Then
            MostrarResumen()
            _mainWindow.ResumenBotones.Children.Add(_mainWindow.FiltrarFechas)
            _mainWindow.FechaAnterior.Text = DateTime.Now.Day.ToString & "/" & DateTime.Now.Month.ToString & "/" & DateTime.Now.Year.ToString
            _mainWindow.FechaPosterior.Text = DateTime.Now.Day.ToString & "/" & DateTime.Now.Month.ToString & "/" & DateTime.Now.Year.ToString
        ElseIf _mainWindow.RadioGrupo.IsChecked = True Then
            MostrarResumen()
            _mainWindow.ResumenBotones.Children.Add(_mainWindow.FiltrarGrupos)
            SQLConexion.CargarGrupos(_mainWindow.comboBoxGrupo)
        ElseIf _mainWindow.RadioCodigo.IsChecked = True Then
            MostrarResumen()
            _mainWindow.ResumenBotones.Children.Add(_mainWindow.FiltrarCodigos)
        End If
    End Sub

    Private Sub MostrarVentanaPedido()
        Dim _pedido As New Pedidos
        _pedido.Show()
    End Sub
#End Region

#Region "CRUD de Clientes"
    Private Sub AgregarCliente()
        Dim _groupNum As Integer = CInt(SeleccionarGrupoCliente.ToString)
        SQLConexion.AgregarClientes(New OCRD() With
                {.cardName = Nombre,
                .groupNum = _groupNum}
                )
        MostrarClientes()
    End Sub

    Private Sub EliminarCliente()
        Dim _cardCode As Integer = CInt(SeleccionarCliente.ToString)
        SQLConexion.EliminarCliente(_cardCode)
        MostrarClientes()
    End Sub

    Private Sub BuscarCliente()
        Dim _cardCode As Integer = CInt(SeleccionarCliente.ToString)
        Dim _cliente As OCRD = SQLConexion.BuscarCliente(_cardCode)

        SQLConexion.CargarGrupos(_mainWindow.CBGrupoCliente)
        Nombre = _cliente.cardName
        SeleccionarGrupoCliente = _cliente.groupNum
    End Sub

    Private Sub ModificarCliente()
        Dim _cardCode As Integer = CInt(SeleccionarCliente.ToString)
        Dim _groupNum As Integer = CInt(SeleccionarGrupoCliente.ToString)

        SQLConexion.ModificarCliente(_cardCode, Nombre, _groupNum)
        MostrarClientes()
    End Sub
#End Region

#Region "CRUD de Grupo de Clientes"
    Private Sub AgregarGrupoClientes()
        SQLConexion.AgregarGrupoClientes(New OCTG() With
                {.groupName = NombreGrupo}
                )
        MostrarGrupoClientes()
    End Sub

    Private Sub EliminarGrupoCliente()
        Dim _groupNum As Integer = CInt(SeleccionarGrupoCliente.ToString)
        SQLConexion.EliminarGrupoClientes(_groupNum)
        MostrarGrupoClientes()
    End Sub

    Private Sub BuscarGrupoClientes()
        Dim _groupNum As Integer = CInt(SeleccionarGrupoCliente.ToString)
        Dim _grupoCliente As OCTG = SQLConexion.BuscarGrupoClientes(_groupNum)

        NombreGrupo = _grupoCliente.groupName
    End Sub

    Private Sub ModificarGrupoClientes()
        Dim _groupNum As Integer = CInt(SeleccionarGrupoCliente.ToString)
        SQLConexion.ModificarGrupoClientes(_groupNum, NombreGrupo)
        MostrarGrupoClientes()
    End Sub
#End Region

#Region "CRUD de Articulos"
    Private Sub AgregarArticulos()
        SQLConexion.AgregarArticulos(New OITM With
                                     {.itemName = NombreArticulo
                                     })
        MostrarArticulos()
    End Sub

    Public Sub EliminarArticulos()
        Dim _itemCode As Integer = CInt(SeleccionarArticulo.ToString)
        SQLConexion.EliminarArticulos(_itemCode)
        MostrarArticulos()
    End Sub

    Public Sub BuscarArticulo()
        Dim _itemCode As Integer = CInt(SeleccionarArticulo.ToString)
        Dim _items As OITM = SQLConexion.BuscarArticulos(_itemCode)
        NombreArticulo = _items.itemName
    End Sub

    Public Sub ModificarArticulo()
        Dim _itemCode As Integer = CInt(SeleccionarArticulo.ToString)
        SQLConexion.ModificarArticulos(_itemCode, NombreArticulo)
        MostrarArticulos()
    End Sub
#End Region

#Region "Reportes "
    Public Sub ReporteResumen()
        Dim _archivadora As New Archivadora
        _archivadora.ResumenPedidos()
        _mainWindow.Anunciar.Text = "Ruta C:\Resumen Pedidos\ResumenPedidos.txt"
    End Sub

    Public Sub ReporteGrupoDetallado()
        Dim _groupNum As Integer = CInt(SeleccionarGrupoCliente.ToString)
        Dim _archivadora As New Archivadora
        _archivadora.FiltrarPorGrupo(_groupNum, NoRegistros)
    End Sub

    Public Sub ReporteFechaDetallado()
        Dim _archivadora As New Archivadora
        Dim _consulta As Boolean = False
        _archivadora.FiltrarPorFechas(FechaAnterior, FechaPosterior, NoRegistros)
    End Sub

    Public Sub ReporteCodigoDetallado()
        Dim _archivadora As New Archivadora
        _archivadora.FiltrarPorCodigo(Codigo, NoRegistros)
    End Sub

    Public Sub DetallePedido()
        Dim _docEntry As Integer = CInt(SeleccionarPedido.ToString)
        Dim _pedido As RDR1 = SQLConexion.BuscarPedidoActual(_docEntry)
        Dim _detallePedido As New DetallePedidos
        _detallePedido.Show()
        _detallePedido.Documento.Text = _pedido.docEntry
        _detallePedido.FechaHora.Text = _pedido.ORDR.docDate
        _detallePedido.Grupo.Text = _pedido.ORDR.OCRD.OCTG.groupName
        _detallePedido.Cliente.Text = _pedido.ORDR.OCRD.cardName
        _detallePedido.Articulo.Text = _pedido.OITM.itemName
        _detallePedido.Total.Text = _pedido.ORDR.docTotal
    End Sub
#End Region

End Class
