﻿Imports System.ComponentModel
Imports SBODemoGT

Public Class PedidoVM
    Implements ICommand, INotifyPropertyChanged
#Region "Variables declaradas"
    Private _pedidos As Pedidos
    Private _pedidosVm As PedidoVM
    Private _seleccionarCliente As New Object
    Private _seleccionarFactura As New Object
    Private _seleccionarArticulo As New Object
    Private _nombreArticulo As String
    Private _horaFecha As String
    Private _noReferencia As Integer
    Private _precioSinIVa As Decimal
    Private _iva As Decimal
    Private _precio As Decimal
    Private _total As Decimal
#End Region

#Region "Constructor de la clase Pedidos"
    Public Sub New(_instance As Pedidos)
        Me._pedidos = _instance
        Me.VCPedidos = Me
        FechaHora = DateTime.Now.Day.ToString & "/" & DateTime.Now.Month.ToString & "/" & DateTime.Now.Year.ToString & " " & DateTime.Now.Hour.ToString() & ":" & DateTime.Now.Minute.ToString() & ":" & DateTime.Now.Second.ToString()
        SQLConexion.CargarComboCliente(_pedidos.CBCliente)
        _pedidos.Principal.Children.Remove(_pedidos.Articulos)
    End Sub
#End Region

#Region "Propiedades de la Clase PedidosVM"
    Public Property VCPedidos
        Get
            Return _pedidosVm
        End Get
        Set(value)
            _pedidosVm = value
        End Set
    End Property

    Public Property SeleccionarCliente
        Get
            Return _seleccionarCliente
        End Get
        Set(value)
            _seleccionarCliente = value
        End Set
    End Property

    Public Property SeleccionarArticulo
        Get
            Return _seleccionarArticulo
        End Get
        Set(value)
            _seleccionarArticulo = value
        End Set
    End Property

    Public Property SeleccionarFactura
        Get
            Return _seleccionarFactura
        End Get
        Set(value)
            _seleccionarFactura = value
        End Set
    End Property

    Public Property FechaHora As String
        Get
            Return _horaFecha
        End Get
        Set(value As String)
            _horaFecha = value
            NotificarCambio(FechaHora)
        End Set
    End Property

    Public Property NoReferencia As Integer
        Get
            Return _noReferencia
        End Get
        Set(value As Integer)
            _noReferencia = value
            NotificarCambio(NoReferencia)
        End Set
    End Property

    Public Property Precio As Decimal
        Get
            Return _precio
        End Get
        Set(value As Decimal)
            _precio = value
            NotificarCambio(Precio)
        End Set
    End Property

    Public Property Total As Decimal
        Get
            Return _total
        End Get
        Set(value As Decimal)
            _total = value
            NotificarCambio(Total)
        End Set
    End Property

    Public Property PrecioSinIva As Decimal
        Get
            Return _precioSinIVa
        End Get
        Set(value As Decimal)
            _precioSinIVa = value
            NotificarCambio(PrecioSinIva)
        End Set
    End Property

    Public Property Iva As Decimal
        Get
            Return _iva
        End Get
        Set(value As Decimal)
            _iva = value
            NotificarCambio(Iva)
        End Set
    End Property

    Public Property NombreArticulo As String
        Get
            Return _nombreArticulo
        End Get
        Set(value As String)
            _nombreArticulo = value
        End Set
    End Property
#End Region

#Region "Interfaz INotifyPropertyChanged / NotificarCambio"
    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Public Sub NotificarCambio(ByVal propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propiedad))
    End Sub

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter
            Case "Generar"
                GenerarPedido()
            Case "Vender"
                AgregarVenta()
            Case "Cobrar"
                CobrarPedido()
        End Select
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function
#End Region

#Region "Funciones para generar pedidos"
    Private Sub GenerarPedido()
        Dim _cardCode As Integer = CInt(SeleccionarCliente.ToString)
        SQLConexion.AgregarPedido(New ORDR With
                {.cardCode = _cardCode,
                .docDate = FechaHora,
                .numAtCard = NoReferencia,
                .docTotal = 0
                })
        MostrarArticulos()
    End Sub

    Private Sub AgregarVenta()
        Dim _docNum As Integer = CInt(SeleccionarFactura.ToString)
        Dim _itemCode As Integer = CInt(SeleccionarArticulo.ToString)
        Dim _articulo As OITM = SQLConexion.BuscarArticulos(_itemCode)
        Dim _x As Decimal = 0
        NombreArticulo = _articulo.itemName
        Iva = Precio * 0.12
        PrecioSinIva = Precio - Iva

        _x = Precio
        _x = _pedidos.Total.Text + _x
        _pedidos.Total.Text = _x

        Dim _datos As New Articulos With {
            .NombreArticulo = NombreArticulo,
            .Iva = Iva,
            .PrecioSinIva = PrecioSinIva}
        _pedidos.DatosVenta.Items.Add(_datos)

        SQLConexion.AgregarVenta(New RDR1 With
                                 {.docNum = _docNum,
                                 .itemCode = _itemCode,
                                 .lineTotal = PrecioSinIva,
                                 .vatSum = Iva
                                 })
    End Sub

    Private Sub CobrarPedido()
        Dim _docNum As Integer = CInt(SeleccionarFactura.ToString)
        SQLConexion.ModificarPedido(_docNum, _pedidos.Total.Text)
        _pedidos.Close()
    End Sub

    Private Sub MostrarArticulos()
        SQLConexion.CargarComboPedido(_pedidos.CBFactura)
        SQLConexion.CargarComboArticulos(_pedidos.CBArticulo)
        _pedidos.Generar.IsEnabled = False
        _pedidos.Principal.Children.Add(_pedidos.Articulos)
    End Sub

#End Region

End Class

Class Articulos
    Private _nombreArticulo As String
    Private _iva As Decimal
    Private _precioSinIva As Decimal

    Public Property PrecioSinIva As Decimal
        Get
            Return _precioSinIva
        End Get
        Set(value As Decimal)
            _precioSinIva = value
        End Set
    End Property

    Public Property Iva As Decimal
        Get
            Return _iva
        End Get
        Set(value As Decimal)
            _iva = value
        End Set
    End Property

    Public Property NombreArticulo As String
        Get
            Return _nombreArticulo
        End Get
        Set(value As String)
            _nombreArticulo = value
        End Set
    End Property
End Class
