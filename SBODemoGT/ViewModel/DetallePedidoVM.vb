﻿Imports System.ComponentModel
Imports SBODemoGT

Public Class DetallePedidoVM
    Implements ICommand, INotifyPropertyChanged
#Region "Variables declaradas de DetallePedidoVM"
    Private _detallePedidos As DetallePedidos
    Private _detallePedidosVM As DetallePedidoVM
    Private _documento As Integer
    Private _fecha As Date
    Private _grupo As String
    Private _cliente As String
    Private _articulo As String
    Private _total As Decimal
    Public _dbo As New SBODemoGTEntities()
#End Region

#Region "Constructor/Propiedades"
    Public Sub New(_instance As DetallePedidos)
        Me._detallePedidos = _instance
        Me.VCDetallePedidos = Me
    End Sub

    Public Property VCDetallePedidos
        Get
            Return _detallePedidosVM
        End Get
        Set(value)
            _detallePedidosVM = value
        End Set
    End Property
#End Region

#Region "Interfaz INotifyPropertyChanged"
    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        Select Case parameter
            Case "Primero"
                MostrarPrimerPedido()
            Case "Siguiente"
                MostrarSiguientePedido()
            Case "Anterior"
                MostrarAnteriorPedido()
            Case "Ultimo"
                MostrarUltimoPedido()
        End Select
    End Sub
#End Region

#Region "Metodos para navegar por los botones"
    Public Sub MostrarSiguientePedido()
        Dim _docEntry As Integer = _detallePedidos.Documento.Text
        Dim _pedido As RDR1 = SQLConexion.BuscarSiguientePedido(_docEntry)
        _detallePedidos.Documento.Text = _pedido.docEntry
        _detallePedidos.FechaHora.Text = _pedido.ORDR.docDate
        _detallePedidos.Grupo.Text = _pedido.ORDR.OCRD.OCTG.groupName
        _detallePedidos.Cliente.Text = _pedido.ORDR.OCRD.cardName
        _detallePedidos.Articulo.Text = _pedido.OITM.itemName
        _detallePedidos.Total.Text = _pedido.ORDR.docTotal
    End Sub

    Public Sub MostrarAnteriorPedido()
        Dim _docEntry As Integer = _detallePedidos.Documento.Text
        Dim _pedido As RDR1 = SQLConexion.BuscarAnteriorPedido(_docEntry)
        _detallePedidos.Documento.Text = _pedido.docEntry
        _detallePedidos.FechaHora.Text = _pedido.ORDR.docDate
        _detallePedidos.Grupo.Text = _pedido.ORDR.OCRD.OCTG.groupName
        _detallePedidos.Cliente.Text = _pedido.ORDR.OCRD.cardName
        _detallePedidos.Articulo.Text = _pedido.OITM.itemName
        _detallePedidos.Total.Text = _pedido.ORDR.docTotal
    End Sub

    Public Sub MostrarUltimoPedido()
        Dim _docEntry As Integer = _detallePedidos.Documento.Text
        Dim query = (From x In _dbo.RDR1 Where x.docEntry >= 1
                     Order By x.docNum Descending Take 1 Select x).ToList
        For Each x In query
            _documento = x.docEntry
            _fecha = x.ORDR.docDate
            _grupo = x.ORDR.OCRD.OCTG.groupName
            _cliente = x.ORDR.OCRD.cardName
            _articulo = x.OITM.itemName
            _total = x.ORDR.docTotal
        Next
        _detallePedidos.Documento.Clear()
        _detallePedidos.Documento.Text = _documento
        _detallePedidos.FechaHora.Text = _fecha
        _detallePedidos.Grupo.Text = _grupo
        _detallePedidos.Cliente.Text = _cliente
        _detallePedidos.Articulo.Text = _articulo
        _detallePedidos.Total.Text = _total
    End Sub

    Public Sub MostrarPrimerPedido()
        Dim _docEntry As Integer = _detallePedidos.Documento.Text
        Dim query = (From x In _dbo.RDR1 Where x.docEntry >= 1
                     Order By x.docNum Ascending Take 1 Select x).ToList
        For Each x In query
            _documento = x.docEntry
            _fecha = x.ORDR.docDate
            _grupo = x.ORDR.OCRD.OCTG.groupName
            _cliente = x.ORDR.OCRD.cardName
            _articulo = x.OITM.itemName
            _total = x.ORDR.docTotal
        Next
        _detallePedidos.Documento.Clear()
        _detallePedidos.Documento.Text = _documento
        _detallePedidos.FechaHora.Text = _fecha
        _detallePedidos.Grupo.Text = _grupo
        _detallePedidos.Cliente.Text = _cliente
        _detallePedidos.Articulo.Text = _articulo
        _detallePedidos.Total.Text = _total
    End Sub
#End Region

End Class
