--DML
--USAR BASE DE DATOS 
USE SBODemoGT 
GO

--INGRESAR ARTICULOS
INSERT INTO OITM(itemName) VALUES('Huawei Y5 LTE Prepago')
INSERT INTO OITM(itemName) VALUES('Samsung Galaxy J2 Prime')
INSERT INTO OITM(itemName) VALUES('SKY 4.0LM')
INSERT INTO OITM(itemName) VALUES('Bmobile AX1035')
INSERT INTO OITM(itemName) VALUES('Samsung Galaxy J2')
INSERT INTO OITM(itemName) VALUES('Verykool SL5550')
INSERT INTO OITM(itemName) VALUES('Samsung Galaxy J3')

--INGRESAR GRUPOS DE CLIENTES
INSERT INTO OCTG(groupName) VALUES('Clientes Nacionales')
INSERT INTO OCTG(groupName) VALUES('Clientes Extranjeros')

--INGRESAR CLIENTES
INSERT INTO OCRD(cardName, groupNum) VALUES('Consumidor Final', 1)
INSERT INTO OCRD(cardName, groupNum) VALUES('Jos� Morente', 1)
INSERT INTO OCRD(cardName, groupNum) VALUES('David Gonz�lez', 1)
INSERT INTO OCRD(cardName, groupNum) VALUES('Mar�a Castillo', 2)

--INGRESAR ENCABEZADO DE PEDIDOS
INSERT INTO ORDR(cardCode, docDate, docTotal, numAtCard)
	VALUES(1, CURRENT_TIMESTAMP, 2000, 4568132)
INSERT INTO ORDR(cardCode, docDate, docTotal, numAtCard)
	VALUES(2, CURRENT_TIMESTAMP, 1000, 9838333)
INSERT INTO ORDR(cardCode, docDate, docTotal, numAtCard)
	VALUES(3, CURRENT_TIMESTAMP, 1500, 1058054)
INSERT INTO ORDR(cardCode, docDate, docTotal, numAtCard)
	VALUES(4, CURRENT_TIMESTAMP, 3000, 4526874)

--INGRESAR DETALLE DE PEDIDO
INSERT INTO RDR1(docNum, itemCode, lineTotal, vatSum)
	VALUES(1, 7, 1760, 240)
INSERT INTO RDR1(docNum, itemCode, lineTotal, vatSum)
	VALUES(2, 1, 880, 120)
INSERT INTO RDR1(docNum, itemCode, lineTotal, vatSum)
	VALUES(3, 2, 1320, 180)
INSERT INTO RDR1(docNum, itemCode, lineTotal, vatSum)
	VALUES(4, 7, 2640, 360)
